/*
 * l3gd20.h
 *
 *  Created on: 18.11.2014
 *      Author: chris
 */

#include <stdint.h>

#ifndef SRC_LIB_DRIVERS_INCLUDE_L3GD20_H_
#define SRC_LIB_DRIVERS_INCLUDE_L3GD20_H_

#define L3GD20_REGISTER_WHO_AM_I        0x0F
#define L3GD20_REGISTER_CTRL_REG1       0x20
#define L3GD20_REGISTER_CTRL_REG2       0x21
#define L3GD20_REGISTER_CTRL_REG3       0x22
#define L3GD20_REGISTER_CTRL_REG4       0x23
#define L3GD20_REGISTER_CTRL_REG5       0x24
#define L3GD20_REGISTER_REFERENCE       0x25
#define L3GD20_REGISTER_OUT_TEMP        0x26
#define L3GD20_REGISTER_STATUS_REG      0x27
#define L3GD20_REGISTER_OUT_X_L         0x28
#define L3GD20_REGISTER_OUT_X_H         0x29
#define L3GD20_REGISTER_OUT_Y_L         0x2A
#define L3GD20_REGISTER_OUT_Y_H         0x2B
#define L3GD20_REGISTER_OUT_Z_L         0x2C
#define L3GD20_REGISTER_OUT_Z_H         0x2D
#define L3GD20_REGISTER_FIFO_CTRL_REG   0x2E
#define L3GD20_REGISTER_FIFO_SRC_REG    0x2F
#define L3GD20_REGISTER_INT1_CFG        0x30
#define L3GD20_REGISTER_INT1_SRC        0x31
#define L3GD20_REGISTER_TSH_XH          0x32
#define L3GD20_REGISTER_TSH_XL          0x33
#define L3GD20_REGISTER_TSH_YH          0x34
#define L3GD20_REGISTER_TSH_YL          0x35
#define L3GD20_REGISTER_TSH_ZH          0x36
#define L3GD20_REGISTER_TSH_ZL          0x37
#define L3GD20_REGISTER_INT1_DURATION   0x38

typedef struct
{
	int16_t x_axis;
	int16_t y_axis;
	int16_t z_axis;
} L3GD20Data_t;

/** @brief Basic module initialization
 *
 * Here internal variables are initialized
 */
int l3gd20_init(void);

/** @brief Read a single 8 bit register
 *
 * @param reg [in] the address of the register to read from
 * @param data [out] the address of the data to be red to
 * @return 0 in case of an error 1 otherwise
 */
int l3gd20_read_register (uint8_t reg, uint8_t *data);

/** @brief write a single 8 bit register
 *
 * @param reg [in] the address of the register to write to
 * @param data [in] data to be written to the register
 * @return 0 in case of an error 1 otherwise
 */
int l3gd20_write_register (uint8_t reg, uint8_t data);

/** @brief reads the temperature from the device
 *
 * @param temp[out] raw temperature value from device
 * @return 0 in case of an error 1 otherwise
 */
int l3gd20_readtemp(int8_t *temp);

/** @brief read the sensor output x,y,z axis raw digits
 *
 * @param data [out] pointer to the ::L3GD20Data_t struct
 * @return 0 in case of an error 1 otherwise
 */
int l3gd20_get_result(L3GD20Data_t *data);
#endif /* SRC_LIB_DRIVERS_INCLUDE_L3GD20_H_ */
