/**
 * @file
 *****************************************************************************
 * @title   logger.h
 * @author  Daniel Schnell (deexschs)
 *
 * @brief   Logging implementation to common logger task
 *******************************************************************************/

#ifndef _LOG_MSG_H
#define _LOG_MSG_H

#ifdef _cplusplus
extern "C"
#endif

/* Includes ------------------------------------------------------------------*/
#include <stddef.h>
#include <stdbool.h>

#include "FreeRTOS.h"
#include "queue.h"

/* defines */

/* Function declarations -----------------------------------------------------*/

bool log_init();
int log_msg(const char* fmt, ...);
size_t log_msg_max();
void log_task_enable(bool enable);
bool log_is_task_enabled(void);

#ifdef _cplusplus
}
#endif

#endif  /* _LOG_MSG_H */

/* EOF */

